# Drush Oracle Driver

A class used by Drush to query/backup/etc. when Drupal is backed by an Oracle database.

The class gets added to the site's autoloader via a classmap entry. See composer.json. 

Everything is working except:
 - sql:create
 - sql:dump
 - sql:sync