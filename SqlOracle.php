<?php

namespace Drush\Sql;

use Consolidation\SiteProcess\Util\Escape;
use Drush\Drush;
use Drush\Exec\ExecTrait;

class SqlOracle extends SqlBase
{
    use ExecTrait;

    public function command()
    {
        return 'sqlplus';
    }

    public function silent()
    {
        return  '-S';
    }

    // @todo hide creds like mysql does.
    public function creds($hide_password = true)
    {
        $db_spec = $dbSpec = $this->getDbSpec();
        return $this->dbSpec['username'] . '/' . $this->dbSpec['password'] . ($this->dbSpec['host'] == 'USETNS' ? '@' . $this->dbSpec['database'] : '@//' . $this->dbSpec['host'] . ':' . ($db_spec['port'] ? $db_spec['port'] : '1521') . '/' . $this->dbSpec['database']);
    }

    // @todo Implement this.
    public function createdbSql($dbname, $quoted = false)
    {
        Drush::logger()->notice("Unable to generate CREATE DATABASE sql for $dbname");
        return false;
    }

  /**
   * Start building the command to run a query.
   *
   * @param $input_file
   *
   * @return array
   */
  public function alwaysQueryCommand($input_file): array
  {
    return [
      $this->command(),
      $this->silent(),
      $this->creds(!$this->getOption('show-passwords')),
      // This removes column header and various helpful things in mysql.
      $this->getOption('extra', $this->queryExtra),
      $this->queryFile,
      Escape::shellArg($input_file),
    ];
  }
    
    public function queryFormat($query)
    {
        // remove trailing semicolon from query if we have it
        $query = preg_replace('/\;$/', '', $query);

        // some sqlplus settings
        $settings[] = "set TRIM ON";
        $settings[] = "set FEEDBACK OFF";
        $settings[] = "set HEADING OFF";
        $settings[] = "set UNDERLINE OFF";
        $settings[] = "set PAGES 0";
        $settings[] = "set PAGESIZE 50000";

        // are we doing a describe ?
        if (!preg_match('/^ *desc/i', $query)) {
            $settings[] = "set LINESIZE 32767";
        }

        // are we doing a show tables ?
        if (preg_match('/^ *show tables/i', $query)) {
            $settings[] = "set HEADING OFF";
            $query = "select object_name from user_objects where object_type='TABLE' order by object_name asc";
        }

        // create settings string
        $sqlp_settings = implode("\n", $settings) . "\n";

        // important for sqlplus to exit correctly
        return "${sqlp_settings}${query};\nexit;\n";
    }

    public function listTables()
    {
        $tables = [];
        $return = $this->alwaysQuery("SELECT TABLE_NAME FROM USER_TABLES"); // WHERE TABLE_NAME NOT IN ('BLOBS','LONG_IDENTIFIERS')
        if ($out = trim($this->getProcess()->getOutput())) {
            $tables = explode(PHP_EOL, $out);
        }
        return $tables;
    }

    /**
     * @inheritDoc
     */
    public function drop($tables)
    {
        if ($tables) {
            foreach ($tables as $table) {
                $sql = "DROP TABLE \"$table\"";
                $return = $this->alwaysQuery($sql);
                if (!$return) {
                    throw new \Exception('Failed to drop table ' . $table);
                }
            }
        }
        return TRUE;
    }

    public function dbExists()
    {
        // @todo More accurate check.
        return TRUE;
    }


}
